#!/usr/bin/env  python3

import os
import struct
import numpy as np
import argparse
import mmap
import matplotlib.pyplot as plt
from numpy.lib.function_base import append
from tqdm import tqdm
import logging as log

log.basicConfig(level=log.INFO, format='%(asctime)s %(levelname)s - %(message)s')

def chunked_read( fobj, chunk_bytes = 4*1024 ):
    """
    Read a file in small chunks
    """
    while True:
        data = fobj.read(chunk_bytes)
        if( not data ):
            break
        else:
            yield data

def normalize_complex_arr(a: np.ndarray):
    """
    Normalizes an ndarray
    """
    a_oo = a - a.real.min() - 1j*a.imag.min() # origin offsetted
    return a_oo/np.abs(a_oo).max()

def read_SC16Q11(byte, convert: bool = True):
    """
    Read & parse SC16Q11 data
    Parameter: convert - convert SC16Q11 data to USRP format
    In: filename
    """
    s = []
    ret = np.ndarray
    for i in range(0, len(byte), 4):
        sig_i = struct.unpack('<h', byte[i:i+2])[0]
        sig_q = struct.unpack('<h', byte[i+2:i+4])[0]
        log.debug(f"dat=({sig_i},{sig_q} )")
        s_out = complex(real=sig_i, imag=sig_q)
        if convert:
            s_out = np.complex64( complex(real=sig_i, imag=sig_q) )
        s.append(s_out)

    ret = np.array(s)
    log.debug(f"Returning {len(ret)} samples")

    return ret


def read_write_data(filename: str, file_output: str) -> np.ndarray:
    """
    Reads the data from the file
    """
    log.info(f"Converting data...")

    fout = open(file=file_output, mode='ab')
    ret = []
    out_dat = None

    # open the file and show tqdm progress bar
    samp_count = 0
    with tqdm( total=os.path.getsize(filename) ) as pbar:
        with open(filename, 'rb') as b:
            for data in chunked_read(b, chunk_bytes = 4*1024):
                out_dat = read_SC16Q11(data)
                samp_count += len(data)
                out_dat.tofile(fout)
                ret.append(out_dat)
                pbar.update(len(data))

    fout.close()
    log.debug(f"Done! File output size={str(samp_count//2//2)} samples")

    return np.array(ret)

def show_data(iqdata: np.ndarray, iqdata_name: str, srate, fftsize, nsamples):
    nsamples = int(nsamples)
    log.info(f"Displaying {iqdata_name} data.\n\tsrate={srate}, fftsz={fftsize}, n_samples={nsamples}")

    # show PSD
    fig = plt.figure()
    plt.psd(iqdata[:nsamples], NFFT=fftsize, Fs=srate)
    plt.title("Power Spectral Density")
    plt.xlabel("Freq")
    plt.xticks(rotation=45)
    plt.show()

    # show spectral
    d_window = np.hanning(fftsize)     # setup windowing
    fig = plt.figure()
    plt.specgram(iqdata[:nsamples], NFFT=fftsize, Fs=srate, sides='twosided', scale='dB', detrend='none', noverlap=5, window=d_window)
    plt.title("Frame: Spectral Density")
    plt.xlabel("Time")
    plt.ylabel("Freq")
    plt.show()


def get_parsers():
    """
    Get file in,out arguments and IQ data display
    """

    # I/O options
    parser_main = argparse.ArgumentParser('main', description="Convert SC16Q11 (bladeRF) data to usable complex<float> data and optionally display.")
    parser_main.add_argument('file_input', type=str, help='Input file')
    parser_main.add_argument('--output', nargs='?', default='converted-output.bin', type=str, help='Output file')
    parser_main.add_argument('--no_convert', action='store_true', help='Do not convert samples (display only)')
    parser_main.add_argument('--no_normalize', dest='normalize', action='store_false', help='Do not normalize the IQ data')
    parser_main.add_argument('--verbose', action='store_true', help='Verbose Output')

    # display IQ data options
    parser_main.add_argument('--display', action='store_true', help='Display the [output] samples')
    parser_main.add_argument('--samplerate', type=float, default=3.84e6, nargs='?', help='')
    parser_main.add_argument('--nsamples', type=int, default=3.84e6//2, nargs='?', help='')
    parser_main.add_argument('--fftsz', type=int, default=2048, help='FFT bin size')

    return parser_main

if __name__ == '__main__':

    args = get_parsers().parse_args()
    if args.verbose:
        log.getLogger().setLevel(log.DEBUG)

    already_displayed = False
    samp_space = np.ndarray

    if args.no_convert:
        samp_space = np.fromfile(args.file_input, np.complex64)
        show_data(samp_space, args.file_input, args.samplerate, args.fftsz, args.nsamples)
        already_displayed = True

    if args.display and os.path.isfile(args.output):
        show_now = input(f"Found output data ({args.output}). Display converted data? [y/n] ").lower().strip()
        if show_now[0] == 'y':
            # load the data
            log.info("Loading data...")
            samp_space = np.fromfile(args.output, np.complex64)
            show_data(samp_space, args.output, args.samplerate, args.fftsz, args.nsamples)
            already_displayed = True


    if not already_displayed:
        convert = input(f"Convert input data ({args.file_input}) and display [y/n] ").lower().strip()

        if convert[0] == 'y':
            log.info(f"Converting {args.filename} to {args.output}. Normalize={args.normalize}")

            open(args.output, 'wb').close() # clear the output file
            data_in_chunks = read_write_data(args.filename, args.output)
            samp_space = np.fromfile(args.output, np.complex64)

            if args.normalize:
                # normalize entire sample space
                # NOTE rewrites ENTIRE file with normalized data
                log.info("Normalizing data...")
                alldata = []
                for c in tqdm(data_in_chunks):
                    for dat in c:
                        alldata.append(dat)

                f_rewrite = open(args.output, 'ab')
                samp_space = np.array(alldata)
                alldata = normalize_complex_arr( samp_space )
                alldata.tofile(f_rewrite)
                f_rewrite.close()

            log.info("Done!")

            show_data(samp_space, args.output, args.samplerate, args.fftsz, args.nsamples)