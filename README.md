![](img/example_display.png)

# Overview

Converts bladeRF-obtained data to useable data for GNU Radio and USRP radio platforms.

```
python -u main.py --help
usage: main [-h] [--no_normalize] [--verbose] [--display] [--samplerate [SAMPLERATE]] [--nsamples [NSAMPLES]] [--fftsz FFTSZ] filename [output]

Convert SC16Q11 (bladeRF) data to usable complex<float> data and optionally display.

positional arguments:
  filename              Input file
  output                Output file

options:
  -h, --help            show this help message and exit
  --no_normalize        Do not normalize the IQ data
  --verbose             Verbose Output
  --display             Display the plot
  --samplerate [SAMPLERATE]
                        Samplerate
  --nsamples [NSAMPLES]
                        How many samples to display
  --fftsz FFTSZ         FFT bin size
```

## Output
```
python3 -u main.py sepp-xtrx-output.bin
2021-11-16 16:23:03,283 INFO - Converting sepp-xtrx-output.bin to converted-output.bin. Normalize=True
2021-11-16 16:23:03,320 INFO - Converting data...
100%|███████████████████████████| 12201984/129352332 [00:09<01:58, 988016.69it/s
2021-11-16 16:24:29,309 INFO - Normalizing data...
100%|███████████████████████████| 31581/31581 [00:03<00:00, 10228.89it/s]
2021-11-16 16:24:34,412 INFO - Done!
```

## Dependencies

`pip3 install numpy matplotlib tqdm --user`

#### Linux

`sudo apt install python3-tk`

# Collect BladeRF Data

BladeRF CLI script examples located in [scripts](./scripts). Run using the following:

```
bladeRF-cli -s brf-script-900M.txt
```